### This is a README.md file for Mobile UI Automation

### This project is executed on Eclipse IDE for Enterprise Java Developers Version 2020-12

### How to pull the repo to the Eclipse IDE

-Pull the repository to Eclipse as per below
-Open Eclipse
-Navigate to GIT perspective - Window->Perspective->Other->Git
-Select Clone a repository icon
-On the URI paste the Repo URI link and Click next until the button Finish is clickable
-Navigate to Java perspective - Window->Perspective->Other->Java
-Import project from existing Git repository that you added above
-Once the import project has been completed, ensure that the below plugins are installed on your Eclipse 
-TestNG plugin
-Buildship Gradle Integration 3.0
-Appium Studio for Eclipse

Right click on the build.gradle file,select Gradle -> Refresh Gradle Project

To Run the project

-Click on Appium Studio on the Menu bar and select Cloud configuration
-Select the seetest.io radio button and add the below access key
-Access Key: eyJhbGciOiJIUzI1NiJ9.eyJ4cC51IjoxMDY1NjA5OCwieHAucCI6MTA2NTYwOTcsInhwLm0iOjE2MTMzNDAxODg2MTcsImV4cCI6MTkyODcwMDY0OSwiaXNzIjoiY29tLmV4cGVyaXRlc3QifQ.niWFJ2FjfG5l6opyBbgWUL8wCPDNqx66WJUqY8uvs64
-Click Test ,to test the connection
-Click Ok to save the cloud configuration
-Click on Window - Perspective -> Other -> Mobile to change to mobile perspective
-On the list of devices,Select the device that has Android Version 9
-Right click on the device and select Open Device
-On the left pane,tick Instrument checkbox and tick Launch without reset checkbox
-Click on the Install icon to install the App
-Once the App has been installed,launch the App
-Navigate to AndroidDemoTest.java
-Right Click on the file and Run As TestNG Test


UI Tests

This Automation tests the basic navigation of the App.

