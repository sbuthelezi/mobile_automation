package com.appium.auto;

import java.net.URL;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;


public class AndroidDemoTest extends BaseTest {
	protected AndroidDriver<AndroidElement> driver = null;
	
	@BeforeMethod
	@Parameters("deviceQuery")
	public void setUp(@Optional("@os='android'") String deviceQuery) throws Exception{
		init(deviceQuery);
		// Init application / devdc.setCapability(MobileCapabilityType.APP, "cloud:com.example.android.uamp.next/com.example.android.uamp.MainActivity");
		dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.example.android.uamp.next");
		dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.example.android.uamp.MainActivity");
		dc.setCapability("appVersion", "1.0");
		dc.setCapability("instrumentApp", true);
		dc.setCapability("testName", "AndroidDemoTest");
		driver = new AndroidDriver<>(new URL(getProperty("url",cloudProperties) + "/wd/hub"), dc);
	}
	
	@Test
	public void test(){
		// Enter the test code
		driver.findElement(in.Repo.obj("UAMPHomePage.Albums")).click();
		driver.findElement(in.Repo.obj("AlbumPage.Cinematic")).click();
		driver.findElement(in.Repo.obj("CinematicPage.The_Story_Unfolds")).click();
	}
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
	
}
