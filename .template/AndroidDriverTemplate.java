public class AndroidDriverTemplate {
	private static final String url = null;
	private static final String accessKey = null;
	private static final String deviceSerialNumber = null;

	private io.appium.java_client.android.AndroidDriver driver;

	public void template() throws Exception {
		final org.openqa.selenium.remote.DesiredCapabilities dc = new org.openqa.selenium.remote.DesiredCapabilities();
		dc.setCapability("accessKey", accessKey);
		dc.setCapability("deviceQuery", "@serialnumber='" + deviceSerialNumber + "'");
		dc.setCapability("dontGoHomeOnQuit", true);
		driver = new io.appium.java_client.android.AndroidDriver<>(new java.net.URL(url + "/wd/hub"), dc);
		try {
			/* {user.code} */
		} finally {
			try {
				driver.quit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
